## Jenkins S3

#### Description

This is an example of jenkins running with Docker. 
We use this project to run jenkins on a kubernetes cluster.
The jenkins home folder will be backed up to S3. 

#### Docker build

```bash
docker build -t ywarezk/nerdeez-jenkins --build-arg AWS_ACCESS_KEY_ID=*** --build-arg AWS_SECRET_ACCESS_KEY=*** --build-arg JENKINS_HOME_S3_BUCKET_NAME=*** .
```

#### Docker run

```bash
docker run -p 3000:8080 -e JENKINS_HOME=/yariv/var/jenkins_home/ -e JENKINS_HOME_S3_BUCKET_NAME=*** -e AWS_ACCESS_KEY_ID=*** -e AWS_SECRET_ACCESS_KEY=*** ywarezk/nerdeez-jenkins
```

#### Backing up

to periodically backup to s3 you need to create a periodic job that will run
```bash
bash backup.sh
```

#### Multiple Replicas

For High Availability you can create multiple replicas of jenkins
but you have to make sure to periodically run a job that does:
```bash
bash restore.sh
```
This will keep your jenkins instances up to data with each other

#### Issues

```bash
stderr: Host key verification failed.
```
This error will be given if you will not create rsa key pair
the Dockerfile already will copy the key to the proper place
but those keys are not in this repository so you will need to create them for your own

```bash
ssh-keygen
```

leave the passphrase empty

