DATE=`date '+%Y-%m-%d:%H:%M:%S'`
tar czf ${JENKINS_HOME}/ecm_jenkins_${DATE}.tar.gz --exclude='*.log' --exclude='${JENKINS_HOME}/jobs/*/builds/*' --exclude='${JENKINS_HOME}/jobs/*/workspace/*' ${JENKINS_HOME}

s3cmd put ${JENKINS_HOME}/ecm_jenkins_${DATE}.tar.gz s3://${JENKINS_HOME_S3_BUCKET_NAME}/ecm_jenkins_${DATE}.tar.gz
rm ${JENKINS_HOME}/ecm_jenkins*.tar.gz
