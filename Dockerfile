FROM jenkins/jenkins:lts

#ARG AWS_SECRET_ACCESS_KEY
#ARG AWS_ACCESS_KEY_ID
ARG JENKINS_HOME
#ARG JENKINS_HOME_S3_BUCKET_NAME

USER root
RUN apt-get update
RUN apt-get install -y s3cmd
COPY backup.sh /usr/local/bin/backup.sh
COPY restore.sh /usr/local/bin/restore.sh

#RUN mkdir -p /yariv/var/jenkins_home/
#RUN mkdir -p /yariv/var/temp
COPY ecm_jenkins.tar.gz /
RUN tar -xvf /ecm_jenkins.tar.gz

RUN chown jenkins:jenkins /yariv -Rf

USER jenkins
RUN mkdir -p /var/jenkins_home/

