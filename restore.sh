#!/usr/bin/env bash

# Fetch backup generated from script above from s3 and restore
# This works best BEFORE you install and start jenkins for the first time
# We use this as part of our jenkins-up logic
FILE=${1:-ecm_jenkins.tar.gz}
echo "using the file $FILE"
cd /
s3cmd get s3://${JENKINS_HOME_S3_BUCKET_NAME}/${FILE}
if [ -f $FILE ]; then
    echo "Archive found, restoring..."
    tar -xvf $FILE
    chown jenkins:jenkins /yariv -Rf
    rm $FILE
else
    echo "No backups found on s3, skipping..."
fi
